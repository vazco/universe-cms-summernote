'use strict';

var toolbar = [
    //[groupname, [button list]]
    ['style', ['style']],
    ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
    ['para', ['paragraph']],
    ['color', ['color']],
    ['fontsize', ['fontsize']],
    ['height', ['height']],
    ['table', ['table']],
    ['lists', ['ul', 'ol']],
    ['insert', ['link', 'picture', 'video', 'hr']],
    ['undoredo', ['undo', 'redo']],
    ['view', ['codeview']]
];

var component = new UniCMS.Component('unicms_summernote', {
    template: 'UniCMSComponentSummernoteOutput',
    schema: {
        type: String
    },
    addInputType: {
        template: 'UniCMSComponentSummernoteInput',
        valueOut: function () {
            return this.code();
        }
    },
    summernoteOptions: {
        toolbar: toolbar
        //airPopover: toolbar
    }
});

UniCMS.registerComponent('summernote', component);
