'use strict';

Package.describe({
    name: 'vazco:universe-cms-summernote',
    version: '0.0.4',
    summary: 'Summernote component for Universe CMS',
    git: 'git@bitbucket.org:vazco/universe-cms-summernote.git',
    documentation: 'README.md'
});

Package.onUse(function (api) {
    api.versionsFrom('1.0.3.2');

    api.use([
        'templating',
        'summernote:standalone@0.6.0_1'
    ], 'client');

    api.use('vazco:universe-cms@0.5.2');

    api.addFiles(['templates.html', 'templates.js', 'templates.css'], 'client');

    api.addFiles('component.js');
});
