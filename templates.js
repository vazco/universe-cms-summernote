'use strict';

Template.UniCMSComponentSummernoteInput.created = function () {
    this.isDirty = new Blaze.ReactiveVar(false);
};

Template.UniCMSComponentSummernoteInput.rendered = function () {
    var component = UniCMS.findComponent();
    var self = this;
    if (!component) {
        console.error('[UniCMS] Summernote view can\'t find component.');
        return;
    }

    this.summernote = this.$('[data-schema-key="' + this.data.name + '"]')
        .summernote(_({}).extend(component.summernoteOptions));

    _.each(component.events, function (handler, key) {
        self.summernote.on('summernote.'+key, handler);
    });

    this.autorun(function () {
        /*
         * This function is preparation for dirty support:
         * when value in minimongo was updated from external source
         * then we should show info and allow user to refresh content
         */
        var tmpl = Template.instance();
        if (tmpl.isDirty.get()) {
            //already dirty
            return;
        }
        if (tmpl.summernote.code() !== tmpl.data.value) {
            //current code differ from data in minimongo
            tmpl.isDirty.set(true);
        }
    });
};

Template.UniCMSComponentSummernoteInput.helpers({
    isDirty: function () {
        return Template.instance().isDirty.get();
    }
});
